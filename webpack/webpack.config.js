const {clientRules, contextPath, isDevelopment, getWebpackPlugins, getOutput, getOptimization} = require("./webpack_settings");

module.exports = {
    context: contextPath,
    entry: {
        index: [
            './src/client/index.js',
        ]
    },
    output: getOutput(),
    mode: isDevelopment() ? 'development' : 'production',
    devtool: "inline-source-map",
    optimization: getOptimization(),
    stats: {
        assets: !isDevelopment(),
        modules: !isDevelopment(),
        chunks: !isDevelopment(),
        children: !isDevelopment(),
        hash: !isDevelopment(),
        entrypoints: !isDevelopment(),
        version: !isDevelopment(),
    },
    module: {
        rules: clientRules
    },
    plugins: getWebpackPlugins()
};