const path = require('path');
const webpack = require("webpack");
const TerserPlugin = require('terser-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');

const contextPath = path.resolve(__dirname, '../');
const isDevelopment = () => !process.env.NODE_ENV || process.env.NODE_ENV === 'development';

const pagePlugin = new HtmlWebpackPlugin({
    title: 'Courageous Dashboard',
    template: "./src/template/index.ejs",
    filename: "./index.html",
    react_id: 'react-entry'
});

const copyPlugin = new CopyPlugin([{
    from: './src/template/favicons/',
    to: 'favicons/'
}]);

// noinspection WebpackConfigHighlighting
module.exports = {
    contextPath,
    isDevelopment,
    getOutput: () => {
        if (!isDevelopment())
            return {
                filename: '[name].[contenthash].bundle.js',
                chunkFilename: "[name].[contenthash].bundle.js",
                path: path.resolve(contextPath, 'dist'),
                publicPath: "/"
            };
        return {
            filename: '[name].js',
            path: path.resolve(contextPath, 'dist'),
            publicPath: "/"
        };
    },
    clientRules: [
        {
            test: /\.m?js(x)?$/,
            exclude: /(node_modules|bower_components)/,
            use: {
                loader: 'babel-loader',
                options: {
                    babelrc: true
                }

            }
        },
        {
            test: /\.(woff(2)?|otf|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
            use: [
                {
                    loader: 'file-loader',
                    options: {
                        name: './font/[name].[ext]',
                    },
                },
            ]
        },
        {
            test: /\.(jpe?g|png|gif|svg)$/i,
            use: [
                {
                    loader: 'file-loader',
                    options: {
                        name: './img/[name].[ext]',
                    },
                },
            ]
        },
        {
            test: /\.css$/,
            use: ['style-loader', 'css-loader']
        }
    ],
    getOptimization: () => {
        if (isDevelopment())
            return {};
        return {
            minimizer: [new TerserPlugin()],
            runtimeChunk: 'single',
            splitChunks: {
                cacheGroups: {
                    vendors: {
                        test: /[\\/]node_modules[\\/]/,
                        name: 'vendors',
                        enforce: true,
                        chunks: 'all'
                    }
                }
            }
        }
    },
    getWebpackPlugins: () => {
        if (isDevelopment()) {
            return [
                new webpack.optimize.AggressiveMergingPlugin(),//Merge chunks
                new CleanWebpackPlugin(),
                pagePlugin,
                copyPlugin
            ];
        } else {
            return [
                new webpack.DefinePlugin({ // <-- key to reducing React's size
                    'process.env': {
                        'NODE_ENV': JSON.stringify('production')
                    }
                }),
                new webpack.optimize.AggressiveMergingPlugin(),//Merge chunks
                new CleanWebpackPlugin(),
                new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
                pagePlugin,
                copyPlugin,
                new webpack.HashedModuleIdsPlugin(),
                new CompressionPlugin({
                    filename: '[path].br[query]',
                    algorithm: 'brotliCompress',
                    test: /\.(js|css|html|svg)$/,
                    compressionOptions: {level: 11},
                    threshold: 10240,
                    minRatio: 0.8,
                    deleteOriginalAssets: true,
                }),
                //new BundleAnalyzerPlugin(),
            ];
        }
    }
};