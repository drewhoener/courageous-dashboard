import os
import sys

import requests
from lxml import html, etree
import json

LOGIN_URL = "http://www.bostonpilots.com/admin/includes/process.php"
URL = "http://www.bostonpilots.com/pilots/index.php"


def main():
    if len(sys.argv) < 3:
        exit(1)
    username = sys.argv[1]
    password = sys.argv[2]

    if username is None or password is None:
        raise ValueError("Username or Password not set")

    session_requests = requests.session()

    # Create payload
    payload = {
        "username": username,
        "password": password,
        "remember": False,
        "form_submission": "login"
    }

    # Perform login
    result = session_requests.post(LOGIN_URL, data=payload, headers=dict(referer=LOGIN_URL))

    # Scrape url
    result = session_requests.get(URL, headers=dict(referer=URL))
    tree = html.fromstring(result.content)

    with open("inbound.html", "wb") as inbound:
        try:
            inbound.write(etree.tostring(tree.xpath("//table[@id='dataTableInbound']")[0]))
        except IndexError or ValueError:
            inbound.close()
            os.remove("inbound.html")
    with open("outbound.html", "wb") as outbound:
        try:
            outbound.write(etree.tostring(tree.xpath("//table[@id='dataTableOutbound']")[0]))
        except IndexError or ValueError:
            outbound.close()
            os.remove("outbound.html")


if __name__ == '__main__':
    main()
