import {createLogger, format, transports} from 'winston';
import moment from 'moment';

const {combine, timestamp, label, printf} = format;
const JSON = require('circular-json');

const print_format = format.printf(({timestamp, level, message, meta}) => {
    /*
    let output = JSON.stringify(message, null, 4);
    if (output && output.startsWith('"') && output.endsWith('"')) {
        output = output.substring(1, output.length - 1);
    }*/
    return `[${moment().format('HH:mm:ss')} ${level.toUpperCase()}] - ${message}${meta ? `\n${JSON.stringify(meta, null, 4)}` : ''}`;
});

const enumerateErrorFormat = format(info => {
    if (info.message instanceof Error) {
        info.message = Object.assign({
            message: info.message.message,
            stack: info.message.stack,
            meta: info.message.stack,
        }, info.message);
    }
    if (info instanceof Error) {
        return Object.assign({
            message: `${info.message} ${JSON.stringify(info.stack, null, 4)}`,
            stack: info.stack,
            meta: info.stack,
        }, info);
    }
    return info;
});

let combined = new transports.File({
    level: 'silly',
    filename: 'log',
    handleExceptions: true,
});

const logger = createLogger({
    transports: [
        new transports.Console({
            level: 'debug',
            handleExceptions: false,
            json: true,
        }),
        combined,
    ],
    exceptionHandlers: [
        combined,
    ],
    format: combine(
        format.splat(),
        format.simple(),
        enumerateErrorFormat(),
        print_format,
    ),
    exitOnError: false,
});

export default logger;