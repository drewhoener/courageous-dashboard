import express from 'express';
import session from 'express-session';
import webpack from 'webpack';
import {stringify} from 'circular-json';
import logger from '../common/logging';
import {sessionkey, mongoUser, mongoPass} from '/opt/misc_projects/darksky.json';
import webpackDevMiddleware from 'webpack-dev-middleware';
import {getTides} from "./parser/tideparser";
import {getRawTables} from "./parser/trafficparser";
import {getSunTimes, getWeatherData} from "./parser/weatherparser";
import {getNOAAData} from "./parser/noaaparser";

const MongoDBStore = require('connect-mongodb-session')(session);
//region constants
const app = express(), STATIC = __dirname;
const expressStaticGzip = require('express-static-gzip');
const webpackConfig = require('../../webpack/webpack.rebuild.config.js');
const webpackCompiler = webpack(webpackConfig);
const bodyParser = require('body-parser');
//endregion

let userStr = '';
if (mongoUser && mongoPass) {
    userStr = `${mongoUser}:${mongoPass}@`;
}

if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {    //Recompile on local changes so we don't have to rebuild
    logger.info('Using Webpack rebuild');
    app.use(webpackDevMiddleware(webpackCompiler, {
        quiet: true,
        noInfo: true,
        stats: {
            assets: false,
            modules: false,
            chunks: false,
            children: false,
            hash: false,
            entrypoints: false,
            version: false,
        },
        publicPath: webpackConfig.output.publicPath
    }));
    //use default static files
}

//parse request bodies
app.use(bodyParser.json());
app.use(session({
    store: new MongoDBStore({
        uri: `mongodb://${userStr}127.0.0.1/courageous?authSource=admin&connectTimeoutMS=10`,
        useNewUrlParser: true,
        databaseName: 'courageous',
        collection: 'sessions'
    }, (error) => logger.info(error)),
    cookie: {
        maxAge: 1000 * 60 * 60 * 24 // 1 day
    },
    secret: sessionkey,
    resave: false,
    saveUninitialized: true
}));

app.post('/notes', (req, res) => {
    const {notes} = req.body;
    if (!notes) {
        res.status(400);
        res.json({error: 'Missing notes body'});
        return;
    }
    if (!Array.isArray(notes) && !isString(notes)) {
        res.status(400);
        res.json({error: 'Notes body must be string or array'});
        return;
    }

    if (!req.session.notes)
        req.session.notes = [];

    req.session.notes = notes;
    res.status(200);
    res.json({notes: req.session.notes});
});

app.get('/notes', (req, res) => {
    if (!req.session.notes)
        req.session.notes = [];
    res.status(200);
    res.json({notes: req.session.notes});
});

app.post('/restrictions', (req, res) => {
    const {restrictions} = req.body;
    if (!restrictions) {
        res.status(400);
        res.json({error: 'Missing restrictions body'});
        return;
    }
    if (!Array.isArray(restrictions.options)) {
        res.status(400);
        res.json({error: 'Restrictions body must be an array'});
        return;
    }

    if (!req.session.restrictions)
        req.session.restrictions = {
            options: [],
            useWindData: true,
            wind: 0
        };

    req.session.restrictions = restrictions;
    res.status(200);
    res.json({notes: req.session.restrictions});
});

app.get('/restrictions', (req, res) => {
    if (!req.session.restrictions)
        req.session.restrictions = {
            options: [],
            useWindData: true,
            wind: 0
        };
    res.status(200);
    res.json({restrictions: req.session.restrictions});
});

app.get('/sun', (req, res) => {
    res.status(200);
    getSunTimes()
        .then(data => {
            res.json(data);
        });
});

app.get('/weather', (req, res) => {
    res.status(200);
    getWeatherData()
        .then(data => {
            res.json(data);
        });
});

app.get('/noaa', (req, res) => {
    res.status(200);
    getNOAAData()
        .then(data => {
            res.json(data);
        });
});

app.get('/traffic', (req, res) => {
    res.status(200);
    getRawTables()
        .then(data => {
            //logger.info(stringify(data, null, 4));
            res.json(data);
        });

});

app.get('/tides', (req, res) => {
    res.status(200);
    getTides().then((data) => {
        logger.info(`found tides`);
        res.json(data);
    });
});

//Respond to get requests, serve the base html file
app.get('*', expressStaticGzip(STATIC, {
    enableBrotli: true
}));

app.listen(3000, () => {
    //db.connect();
    logger.info("Express Server running on port 3000");
});

const isString = (str) => {
    return (typeof str === 'string') || (str instanceof String);
};
