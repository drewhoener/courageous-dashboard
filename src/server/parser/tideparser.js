import axios from 'axios';
import moment from "moment";
import {stringify} from "circular-json";
import logger from '../../common/logging';


//https://tidesandcurrents.noaa.gov/stationhome.html?id=8443838#obs
const stationID = 8443838;
let lastChecked = 0;
const diffToQuery = 1000 * 60 * 2;
let data = null;

function parseTideData(data) {
    if (!data.hasOwnProperty('predictions') || !Array.isArray(data.predictions) || data.predictions.length < 2)
        return null;

    //logger.info(stringify(data.predictions, null, 4));
    let predictions = data.predictions;
    let output = {
        high: [],
        low: [],
    };

    const now = moment();
    for (let tide of predictions) {
        const time = moment(tide.t);
        const timeCopy = moment(tide.t).add(6, 'h');
        if (!time.isValid() || (timeCopy.isValid() && timeCopy.isBefore(now))) {
            //logger.info(`Skipping ${stringify(tide, null, 4)}`);
            continue;
        }
        if (tide.type === 'H')
            output.high.push({
                str: formatTideTime(time),
                time: time.valueOf(),
                type: 'H'
            });
        if (tide.type === 'L')
            output.low.push({
                str: formatTideTime(time),
                time: time.valueOf(),
                type: 'L'
            });
    }
    output.low = output.low.slice(0, 2);
    output.high = output.high.slice(0, 2);
    fillOutput(output.low);
    fillOutput(output.high);
    const name = tagClosest([].concat(output.high).concat(output.low));
    if (name)
        output.nameStr = name;
    return output;
}

function fillOutput(arr) {
    while (arr.length < 2) {
        arr.push({
            str: '---',
            time: 0
        })
    }
}

function tagClosest(arr) {
    let diff = Number.MAX_SAFE_INTEGER;
    let nextTide = null;
    const now = moment();
    for (let elem of arr) {
        const time = moment(elem.time);
        if (!time.isValid() || time.isBefore(now)) {
            continue;
        }
        //logger.info(``);
        const timeDiff = now.valueOf() - time.valueOf();
        if (Math.abs(timeDiff) < diff) {
            nextTide = elem;
            diff = Math.abs(timeDiff);
        }
    }
    if (nextTide)
        nextTide.closest = true;

    if (!nextTide || !nextTide.hasOwnProperty('type'))
        return null;
    if (Math.abs(diff) < 1000 * 60 * 60 * 1.5)
        return 'Slack';
    if (nextTide.type === 'H')
        return 'Flooding';
    if (nextTide.type === 'L')
        return 'Ebbing';
}

function formatTideTime(time) {
    if (!time || !time.isValid())
        return 'Invalid Tide Time';
    return time.calendar(null, {
        lastDay: '[Yesterday at] LT',
        sameDay: 'LT',
        nextDay: '[Tomorrow at] LT',
        lastWeek: '[last] dddd [at] LT',
        nextWeek: 'dddd [at] LT',
        sameElse: 'L'
    });
}

export const getTides = () => {
    //https://tidesandcurrents.noaa.gov/api/datagetter?
    // product=predictions&
    // application=NOS.COOPS.TAC.WL&
    // begin_date=20190810&
    // end_date=20190811&
    // datum=MLLW&
    // station=8443838&
    // time_zone=lst_ldt&
    // units=english&
    // interval=hilo&
    // format=json
    const now = moment().valueOf();
    if (data && (now - lastChecked) < diffToQuery) {
        logger.info(`Not Updating Tides`);
        return Promise.resolve(data);
    }
    const date = moment();
    const tomorrow = moment().add(2, 'd');
    return axios.get(`https://tidesandcurrents.noaa.gov/api/datagetter`, {
        params: {
            station: stationID,
            begin_date: date.format('YYYYMMDD'),
            end_date: tomorrow.format('YYYYMMDD'),
            product: 'predictions',
            datum: 'MLLW',
            units: 'english',
            time_zone: 'lst_ldt',
            format: 'json',
            interval: 'hilo',
            application: 'Courageous_Sailing_Center',
        }
    })
        .then(res => {
            lastChecked = now;
            data = parseTideData(res.data);
            return data;
        }).catch((e) => {
            logger.info(e);
            return {
                error: 'An error occurred retrieving tide data'
            }
        });
};