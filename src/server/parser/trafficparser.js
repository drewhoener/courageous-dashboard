import util from 'util';
import cheerio from 'cheerio';
import moment from 'moment';
import {stringify} from 'circular-json';
import logger from '../../common/logging';

const fs = require('fs');
const exec = util.promisify(require('child_process').exec);

const destinations = [
    'Boston Autoport Charlestown',
    'Boston Autoport Charlestown',
    'Autoport',
    'Irving Revere',
    'Charlestown Marina',
    'Charlestown Marina',
    'Gulf Chelsea',
    'Sunoco',
    'Sea',
    'Schnitzer',
    'Distrigas',
    'Cement',
    'Ciment'
];

const dateFormats = [
    'MM/DD HHmm',
    'MM/DD HH:mm',
    'ddd MM/DD HHmm',
    'ddd MM/DD HH:mm',
    'ddd HHmm',
    'ddd HH:mm'
];

let lastRun = 0;

function extractHeaders($, startPoint) {
    const headers = [];
    startPoint.find('thead tr').children('th').each((id, elem) => {
        let header = $(elem).text().toLowerCase();
        if (header.startsWith('ship'))
            header = 'ship';
        headers.push(header);
    });
    return headers;
}

function extractRows($, startPoint) {
    const rows = [];
    startPoint.find('tbody').children('tr').each((id, elem) => {
        const row = [];
        $(elem).children('td').each((id, rowElem) => {
            const text = $(rowElem).html().split('<br>').filter(elem => elem && elem !== '');
            //logger.info(text);
            row.push(text);
        });
        rows.push(row);
    });
    return rows;
}

function straightenRow(header, content) {
    switch (header) {
        default:
        case 'agent':
        case 'ship':
        case 'from':
        case 'to':
        case 'pilot':
            if (Array.isArray(content) && content.length >= 1)
                content = content[0];
            break;
        case 'eta':
        case 'etd':
            const regex = /\?+/im;
            if (Array.isArray(content)) {
                return content.map(elem => elem.replace(regex, ''));
            }
            return content.replace(regex, '');
    }
    if (Array.isArray(content))
        return content.map(elem => elem.trim());
    return content.trim();
}

function isWatchedDestination(content) {
    //logger.info(`Checking watched destination for ${content}`);
    for (let destination of destinations) {
        if (content.includes(destination))
            return true;
    }
    return false;
}

function parseTug(dates) {
    const regex = /\s*Tug:?\s*/mi;
    if (!Array.isArray(dates))
        dates = [dates];
    let date = null;
    for (let dateStr of dates) {
        if (regex.test(dateStr)) {
            date = dateStr.toString();
            break;
        }
    }
    if (!date)
        return null;
    date = date.replace(regex, '');
    //logger.info(`Tug Date: ${stringify(date, null, 4)}`);
    const tug_data = {date: null};
    parseDate(date, tug_data);
    return tug_data;
}

function parsePOB(dates) {
    const regex = /\s*POB:?\s*/mi;
    if (!Array.isArray(dates))
        dates = [dates];
    let date = dates[0];
    for (let dateStr of dates) {
        if (regex.test(dateStr)) {
            date = dateStr;
            break;
        }
    }

    const date_data = {
        pob: regex.test(date),
        date: null
    };
    date = date.replace(regex, '');
    //logger.info(`POB date: ${stringify(date, null, 4)}`);
    parseDate(date, date_data);
    return date_data;
}

function parseDate(date, obj) {
    for (let format of dateFormats) {
        let momentDate = moment(date, format);
        if (momentDate.isValid()) {
            //logger.info(`Matched format ${format}`);
            obj.date = momentDate;
            obj.str = momentDate.calendar();
            break;
        }
    }
}

function parseTableToJSON($, startPoint) {
    const objects = [];
    const headers = extractHeaders($, startPoint);
    const rows = extractRows($, startPoint);
    const today = moment();
    for (let row of rows) {
        let obj = {};
        let add = true;
        for (let index in headers) {
            if (index >= row.length)
                continue;
            const header = headers[index];
            let content = straightenRow(header, row[index]);
            if (header === 'from' || header === 'to')
                if (!isWatchedDestination(content)) {
                    logger.info(`Removing destination ${content}`);
                    add = false;
                    break;
                }
            if (header === 'eta' || header === 'etd') {
                const date_data = {
                    pob: parsePOB(content),
                    tug: parseTug(content)
                };
                //logger.info(`Date data ${stringify(date_data, null, 4)}`);
                const {pob} = date_data;
                if (pob && pob.date && (pob.date.diff(today, 'hours') > 12 || pob.date.isBefore(today))) {
                    if (Math.abs(pob.date.valueOf() - today.valueOf()) > 3600000) {
                        logger.info(`Date ${date_data.pob.date.toLocaleString()} is not today`);
                        add = false;
                        break;
                    }
                }
                if (pob && pob.date) {
                    pob.date = pob.date.valueOf();
                    content = date_data;
                }
            }
            obj[header] = content;
        }
        if (add)
            objects.push(obj);
    }
    return objects;
}

async function runTrafficScript() {
    const {trafficuser, trafficpass} = require('/opt/misc_projects/darksky.json');
    try {
        const now = moment().valueOf();
        logger.info(`Time Diff is ${now - lastRun}`);
        if (now - lastRun > 300000) {
            lastRun = now;
            await exec(`(cd dist/python; python3 traffic.py ${trafficuser} ${trafficpass})`);
            logger.info(`Fetched latest data`);
        } else {
            logger.info(`Not Updating Traffic`);
        }
    } catch (e) {
        logger.info(`An error occurred while running the traffic python script`);
    }
}

export const getRawTables = () => {
    const inboundPath = 'dist/python/inbound.html';
    const outboundPath = 'dist/python/outbound.html';

    return runTrafficScript()
        .then(() => {
            //check if files exist and load them
            const inboundFile = fs.existsSync(inboundPath) ? fs.readFileSync(inboundPath) : '';
            const inboundData = cheerio.load(`${inboundFile}`);
            const outboundFile = fs.existsSync(outboundPath) ? fs.readFileSync(outboundPath) : '';
            const outboundData = cheerio.load(`${outboundFile}`);
            //If the files are all set, parse the tables into JSON format and send them off
            const inboundJSON = fs.existsSync(inboundPath) ? parseTableToJSON(inboundData, inboundData('table#dataTableInbound')) : null;
            const outboundJSON = fs.existsSync(outboundPath) ? parseTableToJSON(outboundData, outboundData('table#dataTableOutbound')) : null;

            return {
                inbound: inboundJSON,
                outbound: outboundJSON
            }
        })
        .catch((e) => {
            logger.info(`An error occurred`);
            logger.info(e);
            return {
                inbound: null,
                outbound: null
            }
        })
};