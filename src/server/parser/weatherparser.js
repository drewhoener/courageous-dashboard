import {apikey} from '/opt/misc_projects/darksky.json';
import axios from 'axios';
import moment from "moment";
import {stringify} from 'circular-json';
import logger from '../../common/logging';


const latitude = 42.3726;
const longitude = -71.0521;
const queryURL = `https://api.darksky.net/forecast/${apikey}/${latitude},${longitude}`;
let lastChecked = 0;
const diffToQuery = 1000 * 60 * 2;
let data = null;

function getRawWeatherData() {
    const now = moment().valueOf();
    if (data && (now - lastChecked) < diffToQuery) {
        logger.info(`Not Updating Weather`);
        return Promise.resolve(data);
    }
    return axios.get(queryURL, {params: {exclude: 'minutely,flags'}})
        .then(result => {
            //logger.info(result.data);
            lastChecked = now;
            data = result.data;
            return data;
        })
        .catch((e) => {
            logger.info(e);
            return null;
        });
}

export function getWeatherData() {
    return getRawWeatherData()
        .then(weatherData => {
            if (!weatherData)
                return null;
            if (!weatherData.hasOwnProperty('currently') || !weatherData.currently)
                return null;
            const currently = weatherData.currently;
            if (currently.hasOwnProperty('windBearing') && !isNaN(currently.windBearing))
                currently.windDirection = bearingToCompass(currently.windBearing);
            return currently;
        });
}

export function getSunTimes() {
    const sunData = {
        sunrise: 'a',
        sunset: 'a'
    };
    return getRawWeatherData()
        .then(weatherData => {
            if (!weatherData.hasOwnProperty('daily'))
                return sunData;
            const {data} = weatherData.daily;
            if (!data || !Array.isArray(data) || data.length < 1)
                return sunData;

            const dailyData = data[0];
            //logger.info(stringify(dailyData, null, 4));
            if (dailyData.hasOwnProperty('sunriseTime'))
                sunData.sunrise = dailyData.sunriseTime * 1000;

            if (dailyData.hasOwnProperty('sunsetTime'))
                sunData.sunset = dailyData.sunsetTime * 1000;

            return sunData;
        })
        .catch(() => {
            return sunData;
        });
}

function bearingToCompass(bearing) {
    if (isNaN(bearing))
        return '';
    while (bearing >= 360)
        bearing -= 360;
    if (bearing >= 353.75)
        return 'N';
    if (bearing >= 336.25)
        return 'NNW';
    if (bearing >= 313.75)
        return 'NW';
    if (bearing >= 286.25)
        return 'WNW';
    if (bearing >= 258.75)
        return 'W';
    if (bearing >= 236.25)
        return 'WSW';
    if (bearing >= 213.75)
        return 'SW';
    if (bearing >= 191.25)
        return 'SSW';
    if (bearing >= 168.75)
        return 'S';
    if (bearing >= 146.25)
        return 'SSE';
    if (bearing >= 123.75)
        return 'SE';
    if (bearing >= 101.25)
        return 'ESE';
    if (bearing >= 78.75)
        return 'E';
    if (bearing >= 56.25)
        return 'ENE';
    if (bearing >= 33.75)
        return 'NE';
    if (bearing >= 11.25)
        return 'NNE';
    return "N";
}

