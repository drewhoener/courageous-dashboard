import {apikey} from '/opt/misc_projects/darksky.json';
import axios from 'axios';
import moment from "moment";
import {stringify} from 'circular-json';
import logger from '../../common/logging';

//https://api.weather.gov/points/42.3719,-71.0513
//https://api.weather.gov/gridpoints/BOX/71,77/forecast
//https://api.weather.gov/gridpoints/TOP/71,77/forecast

const queryURL = `https://api.weather.gov/gridpoints/TOP/71,77/forecast`;
let lastChecked = 0;
const diffToQuery = 1000 * 60 * 3;
let data = null;

const matchers = [
    {
        key: 'RANGE_MPH',
        regex: /(\d+)\s+to\s+(\d+)\s+mph/i
    },
    {
        key: 'SINGLE_MPH',
        regex: /(\d+)\s+mph/i
    }
];

function getRawNOAAData() {
    const now = moment().valueOf();
    if (data && (now - lastChecked) < diffToQuery) {
        logger.info(`Not Updating NOAA`);
        return Promise.resolve(data);
    }
    return axios.get(queryURL)
        .then(response => {
            lastChecked = now;
            data = response.data;
            return data;
        })
        .catch((e) => {
            logger.info(e);
            return null;
        });
}

export function getNOAAData() {
    return getRawNOAAData()
        .then(data => {
            if (!data)
                return null;
            if (!data.hasOwnProperty('properties') || !data.properties)
                return null;
            const {properties} = data;
            if (!properties.hasOwnProperty('periods'))
                return null;
            const {periods} = properties;
            if (!Array.isArray(periods) || periods.length < 1)
                return null;
            return straightenData(periods[0]);
        });
}

function straightenData(data) {
    if (data.hasOwnProperty('windSpeed'))
        data.windSpeed = convertSpeed(data.windSpeed);
    if (data.hasOwnProperty('detailedForecast'))
        data.detailedForecast = parseForecast(data.detailedForecast);
    return data;
}

function convertSpeed(speedStr) {
    let matcher = null;
    for (let regex of matchers) {
        if (regex.regex.test(speedStr)) {
            matcher = regex;
            break;
        }
    }
    if (!matcher || !matcher.hasOwnProperty('regex') || !matcher.hasOwnProperty('key'))
        return speedStr;
    let groups = matcher.regex.exec(speedStr);
    if (!groups)
        return speedStr;
    switch (matcher.key) {
        case 'RANGE_MPH':
            if (groups.length < 3)
                return speedStr;
            return `${mphToKt(parseInt(groups[1]))} to ${mphToKt(parseInt(groups[2]))} kts`;
        case 'SINGLE_MPH':
            if (groups.length < 2)
                return speedStr;
            return `${mphToKt(parseInt(groups[1]))} kts`;
        default:
            return speedStr;
    }
}

function parseForecast(forecast) {
    let matcher = null;
    while (true){
        for (let regex of matchers) {
            if (regex.regex.test(forecast)) {
                matcher = regex;
                break;
            }
        }
        if (!matcher || !matcher.hasOwnProperty('regex') || !matcher.hasOwnProperty('key'))
            break;
        let groups = matcher.regex.exec(forecast);
        if (!groups || groups.length < 1)
            return forecast;
        forecast = forecast.replace(matcher.regex, convertSpeed(groups[0]));
    }
    return forecast;
}

function mphToKt(mph) {
    if (isNaN(mph))
        return 0;
    return (mph / 1.15078).toFixed(1);
}