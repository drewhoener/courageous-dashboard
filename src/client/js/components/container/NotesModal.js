import React, {Component} from 'react';
import {Button, Form, Modal, TextArea} from "semantic-ui-react";

class NotesModal extends Component {
    constructor(props) {
        super(props);
    }

    getNotesContent() {
        if (!Array.isArray(this.props.notes))
            return '';
        if (this.props.notes.length < 1)
            return '';
        if (this.props.notes.length === 1)
            return this.props.notes[0];
        return this.props.notes.reduce((o, o1) => `${o}\n${o1}`);
    }

    render() {
        return (
            <Modal onClose={this.props.onSubmit} open={this.props.visible} centered={false}>
                <Modal.Header>Edit Notes</Modal.Header>
                <Modal.Content>
                    <Form>
                        <TextArea name={'notes'} value={this.getNotesContent()}
                                  onChange={(evt, data) => this.props.onModify(data)}/>
                    </Form>
                </Modal.Content>
                <Modal.Actions>
                    <Button primary content={'Save'} onClick={this.props.onSubmit}/>
                </Modal.Actions>
            </Modal>
        );
    }
}

export default NotesModal;