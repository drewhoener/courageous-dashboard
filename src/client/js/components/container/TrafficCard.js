import React, {Component} from 'react';
import {Card, Icon} from 'semantic-ui-react';
import moment from "moment";

class TrafficCard extends Component {
    constructor(props) {
        super(props);
    }

    getDestinationIcon(icon) {
        if (this.props.hasOwnProperty('etd')) {
            return (
                <Icon.Group size={'large'}>
                    <Icon name={icon}/>
                    <Icon corner={'top right'} name='arrow right'/>
                </Icon.Group>
            )
        } else {
            return (
                <Icon.Group size={'large'}>
                    <Icon name={icon}/>
                    <Icon corner={'top left'} name='arrow left'/>
                </Icon.Group>
            )
        }
    }

    isLeaving() {
        return this.props.hasOwnProperty('etd');
    }

    hasTug() {
        if (this.props.hasOwnProperty('eta'))
            return this.props.eta.hasOwnProperty('tug') && this.props.eta.tug;
        if (this.props.hasOwnProperty('etd'))
            return this.props.etd.hasOwnProperty('tug') && this.props.etd.tug;
        return false;
    }

    getShipDescription() {
        const description = [];
        const leaving = this.isLeaving();
        let {from, to} = this.props;
        //from = null;
        //to = null;
        if (!from)
            from = leaving ? 'Boston Harbor' : 'Sea';
        if (!to)
            to = leaving ? 'Sea' : 'Boston Harbor';
        if (leaving) {
            description.push(`Departing from ${from} to ${to}`);
        } else {
            description.push(`Arriving at ${to} from ${from}`);
        }

        return description;
    }

    getPOB() {
        const isLeaving = this.isLeaving();
        let pob_data = Object.assign({}, isLeaving ? this.props.etd : this.props.eta);
        if (!pob_data.hasOwnProperty('pob'))
            pob_data.pob = {
                pob: true,
                date: moment().valueOf(),
                str: 'Today, Invalid Time'
            };
        pob_data = Object.assign({}, pob_data.pob);
        const arrStr = isLeaving ? 'Departing' : 'Arriving';
        pob_data.str = pob_data.pob ? `${isLeaving ? `Pilot Onboard` : ''} ${pob_data.str}` : `${arrStr}: ${pob_data.str}`;
        return pob_data;
    }

    getTug() {
        if (!this.hasTug())
            return null;
        const isLeaving = this.isLeaving();
        let time_data = Object.assign({}, isLeaving ? this.props.etd : this.props.eta);
        if (!time_data.hasOwnProperty('tug'))
            time_data.tug = {
                date: moment().valueOf(),
                str: 'Today, Invalid Time'
            };
        time_data = time_data.tug;
        time_data.str = `Tug ${time_data.str}`;
        return time_data;
    }

    render() {
        const {ship} = this.props;
        const tug_data = this.getTug();
        const pob_data = this.getPOB();
        return (
            <Card fluid>
                <Card.Content>
                    <Card.Header>{ship || 'Invalid Name'}</Card.Header>
                    {
                        this.props.agent &&
                        <Card.Meta>{this.props.agent.trim()}</Card.Meta>
                    }
                </Card.Content>
                <Card.Content>
                    <p>
                        {
                            this.getShipDescription().map((elem, idx) => (
                                <React.Fragment key={`${ship}-d-${idx}`}>{elem}<br/></React.Fragment>))
                        }
                    </p>
                </Card.Content>
                <Card.Content extra>
                    <Card.Content>
                        {
                            this.getDestinationIcon('ship')
                        }
                        {
                            pob_data.str
                        }
                    </Card.Content>
                    {
                        tug_data &&
                        <Card.Content>
                            {
                                this.getDestinationIcon('linkify')
                            }
                            {
                                tug_data.str
                            }
                        </Card.Content>
                    }
                </Card.Content>
            </Card>
        );
    }
}

export default TrafficCard;