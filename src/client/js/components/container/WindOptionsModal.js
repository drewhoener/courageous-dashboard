import React, {Component} from 'react';
import {Button, Form, Modal, Radio, Segment} from "semantic-ui-react";

class WindOptionsModal extends Component {
    constructor(props) {
        super(props);

    }

    getOptions() {
        if (!this.props.options)
            return {};
        return this.props.options.filter(elem => {
            return elem.hasOwnProperty('key') && elem.hasOwnProperty('option') && elem.hasOwnProperty('html');
        })
    }

    render() {
        const reverseCheck = (e, d) => {
            d.checked = !d.checked;
            this.props.setOption(e, d);
        };
        const parseWind = (e, d) => {
            let val = d.value;
            if (isNaN(val) || val === '')
                val = '0';
            d.value = parseInt(val);
            this.props.setOption(e, d);
        };
        return (
            <Modal onClose={this.props.onSubmit} open={this.props.visible}>
                <Modal.Header>Set Restrictions</Modal.Header>
                <Modal.Content>
                    <Segment textAlign={'left'}>
                        <Form>
                            <Form.Group>
                                <Form.Field>
                                    <label style={{verticalAlign: 'top'}}>Manual Entry</label>
                                    <Radio checked={!this.props.useWindData} onChange={reverseCheck}
                                           name={`useWindData`} toggle/>
                                </Form.Field>
                                <Form.Input name={'wind'} label='Wind Speed (kts)' type={'number'}
                                            onChange={parseWind} value={this.props.wind === 0 ? '' : this.props.wind}
                                            disabled={this.props.useWindData}/>
                            </Form.Group>
                            {
                                this.getOptions().map(elem => {
                                    return (
                                        <Form.Field inline key={`wind-option-${elem.key}`}>
                                            <Radio checked={this.props.checked.includes(elem.key)}
                                                   onChange={this.props.setOption} name={`option-${elem.key}`} toggle/>
                                            <label style={{verticalAlign: 'top'}}>{elem.option}</label>
                                        </Form.Field>
                                    )
                                })
                            }
                        </Form>
                    </Segment>
                </Modal.Content>
                <Modal.Actions>
                    <Button primary content={'Save'} onClick={this.props.onSubmit}/>
                </Modal.Actions>
            </Modal>
        );
    }
}

export default WindOptionsModal;