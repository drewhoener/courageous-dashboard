import React, {Component} from 'react';
import {Segment} from "semantic-ui-react";
import NotesModal from "../../container/NotesModal";
import axios from "axios";

class NotesDisplay extends Component {
    constructor(props) {
        super(props);
        this.state = {
            notes: [],
            modalVisible: false
        }
    }

    componentDidMount() {
        this.loadNotes();
    }

    loadNotes() {
        axios.get('/notes')
            .then(response => {
                if (response.status === 200) {
                    let {data} = response;
                    if (!Array.isArray(data.notes))
                        return;
                    this.setState({notes: data.notes});
                }
            })
            .catch(error => {
                console.log(error);
                //this.setEmptyData();
            });
    }

    saveNotes = (evt, data) => {
        this.setState({modalVisible: false});
        axios.post('/notes', {notes: this.state.notes})
            .catch(error => {
                console.log(error);
            });
    };

    modifyNotes = (data) => {
        const {value} = data;
        this.setState({notes: value.split('\n')});
    };

    showModal = () => {
        this.setState({modalVisible: true});
    };

    render() {
        return (
            <>
                <NotesModal onModify={this.modifyNotes} onSubmit={this.saveNotes} visible={this.state.modalVisible}
                            notes={this.state.notes}/>
                <Segment className={'massive-larger'} size={'massive'} textAlign={'center'}
                         attached onClick={() => this.showModal()}>
                    Notes
                </Segment>
                <Segment className={'size-notes'} size={'huge'} textAlign={'center'} attached={'bottom'}
                         onClick={() => this.showModal()}>
                    {
                        this.state.notes
                            .filter(elem => elem.length > 1)
                            .map((note, idx) => (<p className={'notes-spacing'} key={`notes-${idx}`}>{note}</p>))
                    }
                </Segment>
            </>
        );
    }
}

export default NotesDisplay;