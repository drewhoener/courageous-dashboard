import React, {Component} from 'react';
import {Segment, Table} from "semantic-ui-react";
import moment from "moment";
import axios from "axios";

class SunDisplay extends Component {
    constructor(props) {
        super(props);
        this.dockTimeOffset = 1800000;
        let regex = `\\d{${moment().valueOf().toString().length}}`;
        this.regex = RegExp(regex);
        this.state = {
            sunrise: 'a',
            sunset: 'a'
        }
    }

    componentDidMount() {
        this.loadSunTimes();
        setTimeout(() => this.refreshSun(), 1000 * 60);
    }

    refreshSun = () => {
        console.log(`Refreshing Sun`);
        this.loadSunTimes();
        setTimeout(() => this.refreshSun(), 1000 * 60);
    };

    setEmptyData = () => {
        this.setState({
            sunrise: 'a',
            sunset: 'a'
        });
    };

    loadSunTimes = () => {
        axios.get('/sun')
            .then(response => {
                let {data} = response;
                this.setState(data);
            })
            .catch(error => {
                this.setEmptyData();
            });
    };

    formatSunTime(time) {
        if (!this.regex.test(time)) {
            return '---';
        }
        const date = moment(time);
        if (!date.isValid())
            return '---';
        return date.format('hh:mm A');
    }

    render() {
        return (
            <>
                <Segment className={'massive-larger'} size={'massive'} textAlign={'center'} attached={'top'}
                         content={'Sun'} compact={this.props.isSmall}/>
                <Table className={'table-font'} definition unstackable size={'large'} attached={'bottom'}
                       compact={this.props.isSmall}>
                    <Table.Body>
                        <Table.Row>
                            <Table.Cell className={'bold'}>Sunrise</Table.Cell>
                            <Table.Cell textAlign={'center'}>{this.formatSunTime(this.state.sunrise)}</Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell className={'bold'}>Dock Time</Table.Cell>
                            <Table.Cell className={'error'}
                                        textAlign={'center'}>{this.formatSunTime(this.state.sunset - this.dockTimeOffset)}</Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell className={'bold'}>Sunset</Table.Cell>
                            <Table.Cell textAlign={'center'}>{this.formatSunTime(this.state.sunset)}</Table.Cell>
                        </Table.Row>
                    </Table.Body>
                </Table>
            </>
        );
    }
}

export default SunDisplay;