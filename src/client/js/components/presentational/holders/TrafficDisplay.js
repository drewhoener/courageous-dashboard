import React, {Component} from 'react';
import {Grid, Card, Segment} from "semantic-ui-react";
import axios from 'axios';
import TrafficCard from "../../container/TrafficCard";
import scrollIntoView from 'scroll-into-view';
import NotesDisplay from "./NotesDisplay";

class TrafficDisplay extends Component {

    constructor(props) {
        super(props);
        this.state = {
            inbound: [],
            outbound: []
        };
        this.bottomRef = React.createRef();
        this.middleRef = React.createRef();
        this.topRef = React.createRef();
    }

    componentDidMount() {
        this.loadTraffic(() => {
            if (!this.props.isSmall)
                setTimeout(() => this.scrollMiddle(), 1000)
        });
        setTimeout(this.refreshTraffic, 1000 * 60 * 5);
    }

    refreshTraffic = () => {
        console.log(`Refreshing Traffic`);
        this.loadTraffic(null);
        setTimeout(() => this.refreshTraffic(), 1000 * 60 * 5);
    };

    loadTraffic = (callback) => {
        axios.get('/traffic')
            .then(result => {
                console.log(result.data);
                this.setState(() => {
                    let {inbound, outbound} = result.data;
                    if (!inbound)
                        inbound = [];
                    if (!outbound)
                        outbound = [];

                    return {
                        inbound,
                        outbound
                    }
                });
                if (callback)
                    callback();
            })
            .catch(error => {
                console.log('An Error occurred');
            });
    };

    static getTrafficCards(list, name = 'inbound') {
        if (!Array.isArray(list) || list.length < 1) {
            return (
                <Card fluid>
                    <Card.Content header='No Data'/>
                    <Card.Content description='There is no scheduled traffic for today'/>
                </Card>
            );
        }
        return list.map((elem, idx) => <TrafficCard key={`${name}-${idx}`} {...elem}/>)
    }

    scrollTop = () => {
        scrollIntoView(this.topRef.current, {time: 8000}, () => {
            //console.log(`Scrolling to Middle`);
            if (!this.props.isSmall)
                this.scrollMiddle();
        });
    };

    scrollMiddle = () => {
        scrollIntoView(this.middleRef.current, {time: 8000}, () => {
            //console.log(`Scrolling to Bottom`);
            if (!this.props.isSmall)
                this.scrollBottom();
        })
    };

    scrollBottom = () => {
        scrollIntoView(this.bottomRef.current, {time: 8000}, () => {
            //console.log(`Scrolling to Top`);
            if (!this.props.isSmall)
                this.scrollTop();
        });
    };

    render() {
        return (
            <Grid.Row stretched className={'row-height'}>
                <Segment compact={this.props.isSmall} textAlign={'center'} size={'massive'} attached={'top'}
                         content={'Traffic'}/>
                <Segment.Group className={'scroll'}>
                    <div ref={this.topRef}/>
                    <Segment className={'inboundHeader'} size='massive' attached>Inbound</Segment>
                    <Segment.Group>
                        {
                            TrafficDisplay.getTrafficCards(this.state.inbound, 'inbound')
                        }
                    </Segment.Group>
                    <Segment size='massive' attached>Outbound</Segment>
                    <Segment.Group>
                        <div ref={this.middleRef}/>
                        {
                            TrafficDisplay.getTrafficCards(this.state.outbound, 'outbound')
                        }
                        <div ref={this.bottomRef}/>
                    </Segment.Group>
                </Segment.Group>
                <NotesDisplay attached={'bottom'}/>
            </Grid.Row>
        );
    }
}

export default TrafficDisplay;