import React, {Component} from 'react';
import {Segment, Table} from "semantic-ui-react";
import axios from "axios";

class TideDisplay extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tides: {
                high: [{str: 'No Data'}],
                low: [{str: 'No Data'}]
            }
        }
    }

    componentDidMount() {
        this.loadTides();
        setTimeout(this.refreshTides, 1000 * 60);
    }

    refreshTides = () => {
        console.log(`Refreshing Tides`);
        this.loadTides();
        setTimeout(() => this.refreshTides(), 1000 * 60);
    };

    setEmptyTides() {
        this.setState({
            tides: {
                high: [{str: 'No Data'}],
                low: [{str: 'No Data'}]
            }
        })
    }

    loadTides = () => {
        axios.get('/tides')
            .then(response => {
                let {data} = response;
                if (!data || !data.high || !data.low) {
                    this.setEmptyTides();
                    return;
                }
                if (!Array.isArray(data.high) || !Array.isArray(data.low)) {
                    this.setEmptyTides();
                    return;
                }
                this.setState({
                    tides: data
                })
            })
            .catch(error => {
                this.setEmptyTides();
            });
    };

    render() {
        return (
            <>
                <Segment compact={this.props.isSmall} size={'massive'} textAlign={'center'} attached={'top'}
                         className={'massive-larger'}>
                    Tide <span
                    className={'smallerText'}>( {this.state.tides.nameStr ? this.state.tides.nameStr : ''} )</span>
                </Segment>
                <Table className={'table-font'} definition unstackable compact={this.props.isSmall} size={'large'}
                       attached={'bottom'}>
                    <Table.Body>
                        <Table.Row>
                            <Table.Cell className={'bold'}>High</Table.Cell>
                            {
                                this.state.tides.high.map((elem, idx) => {
                                    if (elem.closest)
                                        return (<Table.Cell className={'bold'} textAlign={'center'}
                                                            key={`hightide-${idx}`}>{elem.str}</Table.Cell>);
                                    return (<Table.Cell key={`hightide-${idx}`}
                                                        textAlign={'center'}>{elem.str}</Table.Cell>);
                                })
                            }
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell className={'bold'}>Low</Table.Cell>
                            {
                                this.state.tides.low.map((elem, idx) => {
                                    if (elem.closest)
                                        return (<Table.Cell className={'bold'} textAlign={'center'}
                                                            key={`lowtide-${idx}`}>{elem.str}</Table.Cell>);
                                    return (<Table.Cell key={`lowtide-${idx}`}
                                                        textAlign={'center'}>{elem.str}</Table.Cell>);
                                })
                            }
                        </Table.Row>
                    </Table.Body>
                </Table>
            </>
        );
    }
}

export default TideDisplay;