import React, {Component} from 'react';
import {Grid, Icon, Segment} from "semantic-ui-react";
import axios from "axios";
import WindDisplay from "./WindDisplay";

const colors = ['#9abdbf', '#9dd1c7', '#a5e3cd', '#b8f4d0', '#eaffc9', '#eac0a2', '#db837d', '#be475a', '#93003a'];

function getSVG(temp) {
    const celcius = ((temp - 32) * (5 / 9));
    const celciusStr = celcius.toFixed(1);
    const color = mapTempColor(celcius);
    return (
        <div className={'svg-div'}>
            <svg width="100%" height="100%" viewBox="-5 -5 110 110" xmlns="http://www.w3.org/2000/svg">
                <circle cx="50" cy="50" r="50" stroke={color} strokeWidth={3} fillOpacity={0}/>
                <text x="50%" y="50%" fontSize={'2em'} alignmentBaseline={'base'} textAnchor={'middle'}
                      fill="#51c5cf"
                      dy='.25em' dx='-.025em'>{Math.round(temp)}°
                </text>
                <text x="50%" y="75%" fontSize={'.5em'} alignmentBaseline={'base'} textAnchor={'middle'}
                      fill="#51c5cf"
                      dy='.25em' dx='-.4em'>{celciusStr}°C
                </text>
            </svg>
        </div>
    )
}

const mapTempColor = (temp, colorOffset = 0) => {
    const max = 48;
    const min = -20;
    temp = Math.min(Math.max(temp, min), max);
    const x = Math.round((temp - min) / (max - min) * (9 - 1) + 1);

    return colors[Math.min(Math.max(x - 1 + colorOffset, 0), colors.length - 1)];
};

class WeatherDisplay extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tempData: {},
            noaa: {}
        };
    }

    componentDidMount() {
        this.loadWeather();
        setTimeout(() => this.refreshWeather(), 1000 * 60);
    }

    refreshWeather = () => {
        console.log(`Refreshing Weather`);
        this.loadWeather();
        setTimeout(() => this.refreshWeather(), 1000 * 60);
    };

    loadWeather = () => {
        axios.get('/weather')
            .then(response => {
                if (response.status === 200) {
                    let {data} = response;
                    this.setState({tempData: data});
                }
            })
            .catch(error => {
                console.log(error);
                //this.setEmptyData();
            });
        axios.get('/noaa')
            .then(response => {
                if (response.status === 200) {
                    let {data} = response;
                    this.setState({noaa: data});
                }
            })
            .catch(error => {
                console.log(error);
                //this.setEmptyData();
            });
    };

    getWind = () => {
        if (!this.state.tempData) {
            this.refreshWeather();
            return null;
        }
        if (this.state.tempData.hasOwnProperty('windGust'))
            return (this.state.tempData.windGust / 1.15078);
        return null;
    };

    getWindDirection = () => {
        if (!this.state.tempData) {
            this.refreshWeather();
            return '';
        }
        if (this.state.tempData.hasOwnProperty('windDirection'))
            return this.state.tempData.windDirection;
        return '';
    };

    getTemp() {
        if (!this.state.tempData) {
            this.refreshWeather();
            return 70;
        }
        if (this.state.tempData.hasOwnProperty('temperature'))
            return this.state.tempData.temperature;
        if (this.state.tempData.hasOwnProperty('apparentTemperature'))
            return this.state.tempData.apparentTemperature;

        return 34;
    }

    getUV() {
        if (!this.state.tempData) {
            this.refreshWeather();
            return 'Normal? (Missing Data)';
        }
        if (this.state.tempData.hasOwnProperty('uvIndex')) {
            const {uvIndex} = this.state.tempData;
            if (uvIndex >= 9) {
                return (
                    <>
                        <span className={'uv-very-high'}>Very High</span>
                        <span className={'smallerText padded-left'}>( {uvIndex} )</span>
                    </>
                );
            }
            if (uvIndex >= 6) {
                return (
                    <>
                        <span className={'uv-high'}>High</span>
                        <span className={'smallerText padded-left'}>( {uvIndex} )</span>
                    </>
                );
            }
            if (uvIndex >= 3) {
                return (
                    <>
                        <span className={'uv-normal'}>Normal</span>
                        <span className={'smallerText padded-left'}>( {uvIndex} )</span>
                    </>
                );
            }
            return (
                <>
                    <span className={'uv-low'}>Low</span>
                    <span className={'smallerText padded-left'}>( {uvIndex} )</span>
                </>
            );
        }
        return 'Normal? (Missing Data)';
    }

    getSummary() {
        if (!this.state.tempData) {
            this.refreshWeather();
            return 'Clear? (Missing Data)';
        }
        if (this.state.tempData.hasOwnProperty('summary'))
            return this.state.tempData.summary;
        return 'Clear? (Missing Data)';
    }

    getDetailedSummary() {
        if (!this.state.noaa) {
            this.refreshWeather();
            return ['Clear? (Missing Data)'];
        }
        if (this.state.noaa.hasOwnProperty('detailedForecast')) {
            const arr = this.state.noaa.detailedForecast.split('. ')
                .filter(elem => {
                    elem = elem.toLowerCase();
                    if (elem.includes('new rainfall'))
                        return false;
                    if (elem.includes('some of the storms'))
                        return false;
                    return true;
                });
            const flattened = [];
            let curElem = '';
            for (let elem of arr) {
                if (`${curElem}. ${elem}`.length >= 90) {
                    flattened.push(curElem.replace(/^\.\s+/i, ''));
                    curElem = elem;
                    continue;
                }
                curElem = `${curElem}. ${elem}`;
            }
            if (curElem.length > 0)
                flattened.push(curElem.replace(/^\.\s+/i, ''));
            return flattened;
        }
        return ['Clear? (Missing Data)'];
    }

    forecastIcon(icon, size = 'big') {
        const style = {
            color: mapTempColor(this.getTemp(), -2)
        };
        const night = (icon) ? icon.endsWith('night') : false;
        switch (icon) {
            case 'partly-cloudy-night':
            case 'partly-cloudy-day':
                return (
                    <Icon.Group size={size}>
                        <Icon style={style} name={`${night ? 'moon' : 'sun'} outline`}/>
                        <Icon style={style} corner={'bottom right'} name={'cloud'}/>
                    </Icon.Group>
                );
            case 'rain':
                return (
                    <Icon style={style} name={'tint'} size={size}/>
                );
            case 'clear-day':
            case 'clear-night':
            default:
                return (
                    <Icon style={style} name={`${night ? 'moon' : 'sun'} outline`} size={size}/>
                );
        }
    }

    render() {
        return (
            <Segment size={'massive'}>
                <Grid stretched stackable={false}>
                    <Grid.Column width={4}>
                        {
                            getSVG(this.getTemp())
                        }
                    </Grid.Column>
                    <Grid.Column width={12}>
                        <Grid>
                            <Grid.Row>
                                <Grid.Column width={7}>
                                    <Grid.Row verticalAlign={'middle'}>
                                        <Segment textAlign={'center'} className={'bold'}
                                                 size={this.props.isSmall ? 'big' : 'huge'}
                                                 attached={'top'}>
                                            {
                                                this.forecastIcon(this.state.tempData.icon, 'large')
                                            }
                                            Forecast
                                        </Segment>
                                        <Segment textAlign={'center'} className={'page-background'}
                                                 size={this.props.isSmall ? 'big' : 'huge'}
                                                 attached>
                                            {
                                                this.getSummary()
                                            }
                                            <div className={'tiny-text'}>Powered By DarkSky</div>
                                        </Segment>
                                        <Segment textAlign={'center'} className={'bold'}
                                                 size={this.props.isSmall ? 'big' : 'huge'}
                                                 attached>
                                            UV Index
                                        </Segment>
                                        <Segment textAlign={'center'} className={'page-background'}
                                                 size={this.props.isSmall ? 'big' : 'huge'}
                                                 attached={'bottom'}>
                                            {
                                                this.getUV()
                                            }
                                        </Segment>
                                    </Grid.Row>
                                </Grid.Column>
                                <Grid.Column width={9}>
                                    <Grid.Row verticalAlign={'middle'}>
                                        <WindDisplay isSmall={this.props.isSmall} windDirection={this.getWindDirection}
                                                     getWind={this.getWind}/>
                                    </Grid.Row>
                                </Grid.Column>
                            </Grid.Row>
                            <Grid.Row>
                                <Grid.Column width={16}>
                                    <Segment textAlign={'center'} className={'bold'}
                                             size={this.props.isSmall ? 'big' : 'huge'}
                                             attached={'top'}>
                                        Detailed Forecast
                                    </Segment>
                                    <Segment textAlign={'center'} className={'page-background'}
                                             size={this.props.isSmall ? 'big' : 'huge'} attached>
                                        {
                                            this.getDetailedSummary()
                                                .map((elem, idx) => (
                                                    <div key={`detailed-forecast-${idx}`}>{elem}</div>
                                                ))
                                        }
                                    </Segment>
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>
                    </Grid.Column>
                </Grid>
            </Segment>
        )
    }
}

export default WeatherDisplay;