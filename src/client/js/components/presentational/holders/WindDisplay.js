import React, {Component} from 'react';
import {Segment} from "semantic-ui-react";
import WindOptionsModal from "../../container/WindOptionsModal";
import axios from "axios";

const WindOptions = [
    {
        key: 'RED_REEF',
        option: 'Red Flags reef',
        html: (
            <span className={'red-text'}>Red flag members must reef</span>
        )
    },
    {
        key: 'NO_RED',
        option: 'No Red Flags',
        html: (
            <span className={'red-text'}>Red flag members may not go out</span>
        )
    },
    {
        key: 'YELLOW_REEF',
        option: 'Yellow Flags reef',
        html: (
            <span className={'yellow-text'}>Yellow flag members must reef</span>
        )
    },
    {
        key: 'NO_YELLOW',
        option: 'No Yellow Flags',
        html: (
            <span className={'yellow-text'}>Yellow flag members may not go out</span>
        )
    },
    {
        key: 'GREEN_REEF',
        option: 'Green Flags reef',
        html: (
            <span className={'green-text'}>Green flag members must reef</span>
        )
    },
    {
        key: 'NO_SAILING',
        option: 'No Sailing',
        html: (
            <span className={'red-text'}>No Boats will be allowed out</span>
        )
    }
];

class WindDisplay extends Component {
    constructor(props) {
        super(props);
        this.state = {
            options: [],
            modalVisible: false,
            useWindData: true,
            wind: 0
        };
    }

    componentDidMount() {
        this.loadWindOptions();
        setTimeout(() => this.refreshWindOptions(), 1000 * 60)
    }

    loadWindOptions = () => {
        axios.get('/restrictions')
            .then(response => {
                if (response.status === 200) {
                    const newState = {};
                    let body = response.data;
                    if (!body.hasOwnProperty('restrictions'))
                        return;
                    const data = body.restrictions;
                    if (data.hasOwnProperty('options') && Array.isArray(data.options))
                        newState.options = data.options;
                    if (data.hasOwnProperty('useWindData'))
                        newState.useWindData = !!data.useWindData;
                    if (data.hasOwnProperty('wind') && !isNaN(data.wind))
                        newState.wind = data.wind;

                    console.log(newState);

                    this.setState(newState);
                }
            })
            .catch(error => {
                console.log(error);
                //this.setEmptyData();
            });
    };

    refreshWindOptions = () => {
        this.loadWindOptions();
        setTimeout(() => this.refreshWindOptions(), 1000 * 60);
    };

    showModal = () => {
        this.setState({modalVisible: true});
    };

    setWindOption = (evt, data) => {
        console.log(data);
        let {name} = data;
        const isRestriction = name ? name.startsWith('option-') : false;
        if (isRestriction) {
            const {checked} = data;
            name = name.slice(name.indexOf('-') + 1);
            this.setState((state) => {
                const options = [].concat(state.options);
                if (!options.includes(name))
                    options.push(name);
                return {
                    options: options.filter(elem => {
                        if (elem !== name)
                            return true;
                        return checked;
                    })
                };
            })
        } else {
            const value = data.hasOwnProperty('checked') ? data.checked : data.value;
            this.setState(() => {
                return {[name]: value};
            });
        }
    };

    saveWindOptions = () => {
        this.setState({modalVisible: false});
        axios
            .post('/restrictions', {
                restrictions: {
                    options: this.state.options,
                    useWindData: this.state.useWindData,
                    wind: this.state.wind
                }
            })
            .catch(error => {
                console.log(error);
            });
    };

    formatWindSpeed() {
        if (this.state.useWindData) {
            const wind = this.props.getWind();
            if (!wind)
                return '0 Kts? (Missing Data)';
            return `${wind.toFixed(1)} kts`;
        }
        return `${this.state.wind.toFixed(1)} kts`;
    }

    render() {
        return (
            <>
                <WindOptionsModal visible={this.state.modalVisible} options={WindOptions} checked={this.state.options}
                                  useWindData={this.state.useWindData} wind={this.state.wind}
                                  setOption={this.setWindOption}
                                  onSubmit={this.saveWindOptions}/>
                <Segment textAlign={'center'} className={'bold'}
                         size={this.props.isSmall ? 'big' : 'massive'}
                         attached={'top'} onClick={this.showModal}>
                    Wind
                </Segment>
                <Segment textAlign={'center'} className={'page-background'}
                         size={this.props.isSmall ? 'big' : 'massive'}
                         attached={'bottom'} onClick={this.showModal}>
                    <div className={'wind-text'}>
                        <span>{this.formatWindSpeed()}</span>
                        <span className={'padded-left'}>{this.props.windDirection()}</span>
                    </div>
                    {
                        WindOptions
                            .filter(elem => elem.hasOwnProperty('html') && this.state.options.includes(elem.key))
                            .map(elem => {
                                return (
                                    <div key={`wind-restriction-${elem.key}`}>
                                        <p>
                                            {elem.html}
                                        </p>
                                    </div>
                                );
                            })
                    }
                </Segment>
            </>
        );
    }
}

export default WindDisplay;