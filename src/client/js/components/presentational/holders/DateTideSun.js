import React, {Component} from 'react';
import {Grid, Responsive, Segment} from 'semantic-ui-react';
import TideDisplay from "./TideDisplay";
import SunDisplay from "./SunDisplay";
import NotesDisplay from "./NotesDisplay";
import WeatherDisplay from "./WeatherDisplay";
import DateDisplay from "./DateDisplay";

class DateTideSun extends Component {
    constructor(props) {
        super(props);
        this.maxWidth = 1920;
        this.state = {
            width: 1000,
            isSmall: false,
        }
    }

    componentDidMount() {
        this.updateScreen(null, {width: window.innerWidth});
    }

    updateScreen = (event, {width}) => {
        console.log(width);
        this.setState({
            width: width,
            isSmall: width < this.maxWidth
        });
    };

    render() {
        return (
            <Responsive as={Grid} onUpdate={this.updateScreen}>
                <Grid.Column>
                    <Grid.Row color={'teal'}>
                        <Segment.Group>
                            <DateDisplay/>
                            <WeatherDisplay isSmall={this.state.isSmall}/>
                            <Segment.Group horizontal className={'expanded'}>
                                <Segment>
                                    <SunDisplay isSmall={this.state.isSmall}/>
                                </Segment>
                                <Segment>
                                    <TideDisplay isSmall={this.state.isSmall}/>
                                </Segment>
                            </Segment.Group>
                        </Segment.Group>
                    </Grid.Row>
                </Grid.Column>
            </Responsive>
        );
    }
}

export default DateTideSun;