import React, {Component} from 'react';
import {Segment} from "semantic-ui-react";
import moment from 'moment';

class DateDisplay extends Component {
    constructor(props) {
        super(props);
        this.state = {
            time: 0
        }
    }

    componentDidMount() {
        setTimeout(() => this.updateTime(), 500);
    }

    updateTime = () => {
        this.setState({time: Date.now()});
        setTimeout(() => this.updateTime(), 500);
    };

    render() {
        return (
            <Segment className={'date-text'} textAlign={'center'}
                     size={'massive'} raised>
                <div>
                    <div className={'date-display'} style={{paddingRight: '.5em'}}>
                        {moment(this.state.time).format('dddd, MMMM Do, YYYY')}
                    </div>
                    <div className={'date-display'} style={{paddingLeft: '.5em'}}>
                        {moment(this.state.time).format('hh:mm A')}
                    </div>
                </div>
            </Segment>
        );
    }
}

export default DateDisplay;