import React, {Component} from 'react';
import {Dimmer, Grid, Loader} from 'semantic-ui-react';
import DateTideSun from "../holders/DateTideSun";

class LeftDashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            embedURL: ''
        };
        this.embedSrc = 'https://embed.windy.com/embed2.html?lat=42.357&lon=-71.101&zoom=8&level=surface&overlay=radar&menu=&message=&marker=&calendar=&pressure=&type=map&location=coordinates&detail=&detailLat=42.357&detailLon=-71.101&metricWind=default&metricTemp=default&radarRange=-1';
    }

    componentDidMount() {
        window.addEventListener('load', this.handleLoad);
    }

    componentWillUnmount() {
        window.removeEventListener('load', this.handleLoad);
    }

    handleLoad = () => {
        this.setState({
            loading: false,
            embedURL: this.embedSrc
        });
    };

    render() {
        return (
            <>
                <Grid.Row className={'collapsing'}>
                    <DateTideSun/>
                </Grid.Row>
                <Grid.Row stretched>
                    <Dimmer.Dimmable className={'radar'}>
                        <Dimmer active={this.state.loading}>
                            <Loader/>
                        </Dimmer>
                        <iframe className={'radar'} src={this.state.embedURL}
                                frameBorder="0">
                            Your browser does not support iframes
                        </iframe>
                    </Dimmer.Dimmable>
                </Grid.Row>
            </>
        );
    }
}

export default LeftDashboard;