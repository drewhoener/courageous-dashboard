import React, {Component} from 'react';
import {Grid, Responsive} from "semantic-ui-react";
import LeftDashboard from "./LeftDashboard";
import TrafficDisplay from "../holders/TrafficDisplay";

class Dashboard extends Component {
    constructor(props) {
        super(props);
        this.maxWidth = 1920;
        this.state = {
            width: 1000,
            isSmall: false,
        }
    }

    componentDidMount() {
        this.updateScreen(null, {width: window.innerWidth});
    }

    updateScreen = (event, {width}) => {
        console.log(width);
        this.setState({
            width: width,
            isSmall: width < this.maxWidth
        });
    };

    render() {
        return (
            <Responsive as={'div'} onUpdate={this.updateScreen} className={'margin-container'}>
                <Grid columns={2} stretched divided>
                    <Grid.Column width={12}>
                        <LeftDashboard/>
                    </Grid.Column>
                    <Grid.Column width={4}>
                        <TrafficDisplay isSmall={this.state.isSmall}/>
                    </Grid.Column>
                </Grid>
            </Responsive>
        );
    }
}

export default Dashboard;