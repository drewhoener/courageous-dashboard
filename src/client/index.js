import Dashboard from "./js/components/presentational/grids/Dashboard";

require('./css/style.css');

import React from "react";
import {render} from 'react-dom';

const entryPoint = document.getElementById('react-entry');
if (entryPoint) {
    render(
        <Dashboard/>,
        entryPoint
    );
}